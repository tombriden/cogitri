# Copyright 2017-2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'telegram-desktop-1.1.17.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

SCM_REPOSITORY="https://github.com/telegramdesktop/tdesktop.git"
SCM_TAG="v${PV}"
SCM_crl_REPOSITORY="https://github.com/telegramdesktop/crl.git"
SCM_gsl_REPOSITORY="https://github.com/Microsoft/GSL.git"
SCM_libtgvoip_REPOSITORY="https://github.com/telegramdesktop/libtgvoip.git"
SCM_variant_REPOSITORY="https://github.com/mapbox/variant.git"
SCM_SECONDARY_REPOSITORIES="crl gsl libtgvoip variant"
SCM_variant_EXTERNAL_REFS=".mason: "
SCM_EXTERNAL_REFS="Telegram/ThirdParty/Catch: Telegram/ThirdParty/GSL:gsl
    Telegram/ThirdParty/libtgvoip:libtgvoip Telegram/ThirdParty/variant:variant
    Telegram/ThirdParty/crl:crl"

require scm-git
require cmake [ api=2 ]
require toolchain-funcs
require gtk-icon-cache freedesktop-desktop

export_exlib_phases pkg_pretend src_prepare src_install pkg_postrm pkg_postinst

SUMMARY="Telegram Desktop messaging app"
HOMEPAGE="https://desktop.telegram.org/"

LICENCES="GPL-3-with-openssl-exception"
SLOT="0"

MYOPTIONS="
    ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-cpp/catch
        dev-cpp/range
        dev-libs/libappindicator:0.1
        media-libs/openal
        media-libs/opus
        media-sound/pulseaudio
        x11-dri/libdrm
        x11-libs/gtk+:3
        x11-libs/libva[X]
        x11-libs/libX11
        x11-libs/qtbase:5[gtk][gui]
        x11-libs/qtimageformats:5
        providers:ffmpeg? ( media/ffmpeg )
        providers:libav? ( media/libav )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # Custom builds, done via a downstream build system don't produce
    # meaningful crash reports for upstream
    -DENABLE_CRASH_REPORTS:BOOL=FALSE

    -DENABLE_GTK_INTEGRATION:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE'
)

CMAKE_SOURCE="${CMAKE_SOURCE}/Telegram"

telegram-desktop_pkg_pretend() {
    if cc-is-gcc && ! ever at_least 7 $(gcc-version) ; then
        eerror "sys-devel/gcc[>=7] is required to build telegram-desktop."
        die
    fi
}

telegram-desktop_src_prepare() {
    # Don't use cmake_src_prepare here, patches expect us to be in ${WORKBASE}/${PNV}
    edo pushd "${WORKBASE}"/${PNV}

    default

    edo mkdir Telegram/cmake
    edo cp "${FILES}"/Telegram.cmake Telegram/CMakeLists.txt
    edo cp "${FILES}"/TelegramCodegen{,Tools}.cmake \
        "${FILES}"/FindBreakpad.cmake \
        "${FILES}"/TelegramTests.cmake \
        Telegram/cmake
    edo cp "${FILES}"/ThirdParty-libtgvoip.cmake Telegram/ThirdParty/libtgvoip/CMakeLists.txt
    edo cp "${FILES}"/ThirdParty-libtgvoip-webrtc.cmake \
        Telegram/ThirdParty/libtgvoip/webrtc_dsp/webrtc/CMakeLists.txt
    edo cp  "${FILES}"/ThirdParty-crl.cmake Telegram/ThirdParty/crl/CMakeLists.txt

    edo popd
}

telegram-desktop_src_install() {
    cmake_src_install

    for size in 16 32 48 64 128 256 512 ; do
        insinto /usr/share/icons/hicolor/${size}x${size}/apps
        newins "${CMAKE_SOURCE}"/Resources/art/icon${size}.png telegram.png
    done

}

telegram-desktop_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

telegram-desktop_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

