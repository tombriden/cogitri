# Copyright 2012 NAKAMURA Yoshitaka
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require python [ blacklist=none multibuild=false with_opt=true ] \
    systemd-service [ systemd_files=[ contrib/${PN}.{socket,service} ] ] \
    openrc-service

export_exlib_phases src_prepare src_install

SUMMARY="validating, recursive, and caching DNS resolver"
HOMEPAGE="https://nlnetlabs.nl/projects/unbound/about/"
DOWNLOADS="https://nlnetlabs.nl/downloads/unbound/${PNV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    debug
    dnscrypt [[ description = [ Support dnscrypt via libsodium ] ]]
    minimal [[ description = [ Only build libunbound, don't build daemon and tools ] ]]
    systemd
    threads
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# Unbound with anything other thanopenssl/libressl fails to compile for me, version 1.6.8
#providers:nss? ( dev-libs/nss )
#providers:nettle? ( dev-libs/nettle )
DEPENDENCIES="
    build+run:
        dev-libs/expat
        dev-libs/libevent[>=1.4.3]
        group/unbound
        user/unbound
        dnscrypt? ( dev-libs/libsodium )
        providers:openssl? ( dev-libs/openssl[>=1.0.0] )
        providers:libressl? ( dev-libs/libressl:= )
        python? ( dev-lang/swig[>=2.0.1][python] )
        systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-dsa
    --enable-ecdsa
    --enable-gost
    --enable-subnet
    # Don't mess with my CFLAGS, please
    --disable-flto
    --disable-static
    --with-libevent=/usr/$(exhost --target)
    --with-libexpat=/usr/$(exhost --target)
    --with-pidfile=/run/unbound.pid
    --with-ssl=/usr/$(exhost --target)
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'debug'
    'systemd'
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'threads pthreads'
    'minimal libunbound-only'
    'python pythonmodule'
    'python pyunbound'
)

unbound_src_prepare() {
    # fix systemd service
    edo sed \
        -e 's:/unbound:/unbound -d:g' \
        -e 's:notify:simple:g' \
        -i contrib/${PN}.service.in

    default
}

unbound_src_install() {
    default

    install_systemd_files
    install_openrc_files
}

