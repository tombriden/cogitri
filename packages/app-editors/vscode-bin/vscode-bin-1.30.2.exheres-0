# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Code editing. Redefined."
HOMEPAGE="https://code.visualstudio.com/"

DOWNLOADS="
    platform:amd64? ( https://vscode-update.azurewebsites.net/${PV}/linux-x64/stable -> ${PNV}-amd64.tar.gz )
"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="platform: amd64"
PLATFORMS="~amd64"

DEPENDENCIES="
    run:
        dev-libs/atk
        dev-libs/expat
        dev-libs/glib:2
        dev-libs/libsecret:1
        dev-libs/nspr
        dev-libs/nss
        gnome-platform/GConf:2
        media-libs/fontconfig
        media-libs/freetype:2
        net-print/cups
        sys-apps/dbus
        sys-libs/glibc
        sys-libs/libgcc:*
        sys-libs/libstdc++:*
        sys-libs/zlib
        sys-sound/alsa-lib
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3
        x11-libs/libX11
        x11-libs/libXScrnSaver
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXtst
        x11-libs/libxcb
        x11-libs/libxkbfile
        x11-libs/pango
"

WORK="${WORKBASE}/VSCode-linux-x64"

pkg_setup() {
    exdirectory --allow /opt
}

src_install() {
    dodir /opt/${PN}
    edo mv "${WORK}"/* "${IMAGE}"/opt/${PN}

    dodir /usr/$(exhost --target)/bin
    dosym /opt/${PN}/code /usr/$(exhost --target)/bin/vscode

    # vscode's tar archive contains neither the desktop file
    # nor an icon. These are from their .deb
    insinto /usr/share/applications
    doins "${FILES}"/vscode.desktop

    insinto /usr/share/pixmaps
    doins "${FILES}"/vscode.png
}

